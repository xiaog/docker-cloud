#!/usr/bin/env bash

TAG=${1}
export BUILD_NUMBER=${TAG}
pwd
mkdir /tmp/.generated

for f in ci/*.yaml
do
 envsubst < $f > "/tmp/.generated/$(basename $f)"
done
cat /tmp/.generated/*
/usr/local/bin/kubectl --kubeconfig kube_config replace -f /tmp/.generated/
rm /tmp/.generated/*
rmdir /tmp/.generated


